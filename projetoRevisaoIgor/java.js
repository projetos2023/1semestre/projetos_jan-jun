const {createApp} = Vue;

createApp({
    data(){
        return{
            username: "",
            password: "",
            error: null,
            sucesso:null,
            
            usuarios: ["admin", "vinicius"],
            senhas:["1234", "1234"],

 
            newUsername: "",
            newPassword: "",
            confirmPassword: "",
            
            mostrarLista: false,
            mostrarCadastro: true,


        }
    }, 

    methods:{
        loginArray(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }
            
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;

                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso!";
                    if (this.username === "admin")
                    {
                        this.mostrarLista = true;
                        this.mostrarCadastro = false;                   
                    }

                    else
                    {
                        this.mostrarLista = false;
                        this.mostrarCadastro= true;
                    }



                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);

                }
                else{
                    this.sucesso = null;
                    this.error = "Usuário ou senha incorretos!";
                }

                this.username = "";
                this.password = "";
                
            }, 1000);



        },

        adicionarUsuario(){
            this.mostrarEntrada = false;

            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }

            setTimeout(() => {
                this.mostrarEntrada = true;            
                if(!this.usuarios.includes(this.newUsername) && this.newUsername !== ""){
                    if(this.newPassword && this.newPassword === this.confirmPassword){
                        this.usuarios.push(this.newUsername);
                        this.senhas.push(this.newPassword);

                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));
                        
                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                        this.sucesso = "Usuário cadastrado com sucesso!";
                        this.error = null;
                    } 
                    else{
                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                        this.error = "Por favor, digite uma senha válida!";
                        this.sucesso = null;
                    }
                } 
                else{
                    this.error = "Por favor, informe um usuário válido!";
                    this.sucesso = null;
                    this.newUsername = "";
                    this.newPassword = "";
                    this.confirmPassword = "";
                } 
            }, 500);             
        }, 

        verCadastrados(){
            this.username = localStorage.getItem("username") || "";
            this.password = localStorage.getItem("password") || "";

            this.mostrarEntrada = false;
            if (this.username === "admin") {
                if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                    this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                    this.senhas = JSON.parse(localStorage.getItem("senhas"));
                }//Fechamento if
                this.mostrarLista = !this.mostrarLista;               
            }//Fechamento if
            else{                
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "Sem privilégios ADMIN!!!";
                }, 400);                
            }//Fechamento else            
        },//Fechamento verCadastrados

        paginaCadastro(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;
                this.sucesso = "Carregando página de cadastro!";
                this.error = null;                
            }, 400);

            setTimeout(() => {
                window.location.href = "cadastro.html";
            }, 2000);

        },

        excluirUsuario(usuario) {
            this.mostrarEntrada = false;

            if (usuario === "admin") {
              // Impedir a exclusão do usuário "admin"
              setTimeout(() => {
                this.mostrarEntrada = true;
                this.sucesso = null;
                this.error = alert("O usuário admin não pode ser excluído!!!");

              }, 1000);
              return;
            }//Fechamento if
      
            if (confirm("Tem certeza que deseja excluir o usuário?")) {
              const index = this.usuarios.indexOf(usuario);
              if (index !== -1) {
                this.usuarios.splice(index, 1);
                this.senhas.splice(index, 1);
      
                // Atualiza os usuários no localStorage
              localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
              localStorage.setItem("senhas", JSON.stringify(this.senhas));
              }
            }//Fechamento if
          },//Fechamento excluirUsuario2

    }, //Fechamento methods
}).mount("#app"); // Fechamento createApp
